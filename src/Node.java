import java.util.LinkedList;
import java.util.StringTokenizer;


public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node() {
        this.name = null;
        this.firstChild = null;
        this.nextSibling = null;
    }

    Node(String name, Node child, Node sibling) {
        this.name = name;
        this.firstChild = child;
        this.nextSibling = sibling;
    }

    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setNextSibling(Node nextSibling) {
        this.nextSibling = nextSibling;
    }

    public Node getNextSibling() {
        return nextSibling;
    }

    public void setFirstChild(Node firstChild) {
        this.firstChild = firstChild;
    }

    public Node getFirstChild() {
        return firstChild;
    }

    public static boolean includesForbiddenCharacter(String s) {
        return s.contains(" ") || s.contains(",") || s.contains("(") || s.contains(")");
    }

    @Override
    public String toString() {
        return leftParentheticRepresentation();
    }

    public static Node parsePostfix(String s) {

        if (s == null) {
            throw new RuntimeException("Node creation error: Argument null");
        }
        s = s.trim();
        if (s.equals("")) {
            throw new RuntimeException("Node creation error: Argument empty string");
        }

        if (!s.contains("(")) {
            String[] strArr = s.trim().split(",");
            if (strArr.length > 1) {
                throw new RuntimeException("Node creation error: Too many root elements in string " + "'" + s + "'");
            }
        } else {
            int count = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '(') {
                    count++;
                }
                if (s.charAt(i) == ')') {
                    count--;
                }
                if (count == 0 && s.charAt(i) == ',') {
                    throw new RuntimeException("Node creation error: Comma error on wrong level : " + "'" + s + "'");
                }
            }
            if (count != 0) {
                throw new RuntimeException("Node creation error: Uneven child elements : " + "'" + s + "'");
            }
        }

        LinkedList<Node> nodes = new LinkedList<>();
        Node node = new Node();
        String previousToken = "";
        StringTokenizer st = new StringTokenizer(s, "(),", true);

        while (st.hasMoreTokens()) {
            String token = st.nextToken(); // .trim();
            if (token.contains(",")) {
                token = token.trim();
            }
            if (token.length() == 0) {
                throw new RuntimeException("Node error: Node name empty in string : " + "'" + s + "'");
            }
            switch (token) {
                case "(":
                    nodes.push(node);
                    node.setFirstChild(new Node());
                    node = node.getFirstChild();
                    break;
                case ",":
                    if (previousToken.equals(token)) {
                        throw new RuntimeException("Node error: Invalid subsequent characters : " + "'" + s + "'");
                    }
                    node.setNextSibling(new Node());
                    node = node.getNextSibling();
                    break;
                case ")":
                    node = nodes.pop();
                    break;
                default:
                    if (!includesForbiddenCharacter(token)) {
                        node.setName(token);
                    } else {
                        throw new RuntimeException("Node creation error: Invalid Node name " + "'" + token + "'" + "\n" + "Original string " + s);
                    }
                    break;

            }
            previousToken = token;
        }
        if (node.getName() == null) {
            throw new RuntimeException("Node error: Invalid Node name : " + "'" + s + "'");
        }
        if (node.getNextSibling() != null) {
            throw new RuntimeException("Node error: Two root elements from string : " + "'" + s + "'");
        }
        if (node.getFirstChild() != null && node.getFirstChild().getName() == null) {
            throw new RuntimeException("Node error: Child Node name missing. String : " + "'" + s + "'");
        }

        return node;
    }

    public String leftParentheticRepresentation() {
        StringBuilder sb = new StringBuilder();
        return leftParentheticRepresentation(sb); // TODO!!! return the string without spaces
    }

    public String leftParentheticRepresentation(StringBuilder sb) {

        sb.append(getName());
        Node childNode = getFirstChild();
        if (childNode != null) {
            sb.append("(");
            while (childNode != null) {
                String delim = (childNode.getNextSibling() != null) ? "," : "";
                sb.append((childNode).leftParentheticRepresentation()).append(delim);
                childNode = childNode.getNextSibling();
            }
            sb.append(")");
        }
        return sb.toString();
    }

    public static Node createTree() {
        return new Node("+",
                new Node("*",
                        new Node("/",
                                new Node("6",
                                        new Node("3", null, null),
                                        null), null),
                        new Node("-",
                                new Node("4", null, null),
                                new Node("2",
                                        new Node("1", null, null),
                                        null))), null);
    }


    public static void main(String[] param) {
        Node r = createTree();
        System.out.println(r.leftParentheticRepresentation());
        // String s = "(B1,C)A";
        String s = "(C,1)AS";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}

// Viited:
// https://moodle.taltech.ee/mod/forum/discuss.php?d=105216
// https://enos.itcollege.ee/~jpoial/algoritmid/puud.html

